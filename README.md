# 序言
在这里，南北一体我们共同书写团聚的故事，携手描画相会的场面，在时空隧道里连接起过去与未来，在斗转星移间铭记日月和点滴。

 

# 校友卡用户注册
 
![输入图片说明](https://images.gitee.com/uploads/images/2021/1011/133016_ca58c13e_9601161.png "屏幕截图.png")


 

 

- 您可以选择人脸识别和手动输入两种方式进行注册

- Tip：校友E卡卡面显示的信息为注册时填写的学籍信息

 
![输入图片说明](https://images.gitee.com/uploads/images/2021/1011/133019_4cec6533_9601161.png "屏幕截图.png")
 
![输入图片说明](https://images.gitee.com/uploads/images/2021/1011/133022_aad9c4f3_9601161.png "屏幕截图.png")
 

 

### 【自动通过】
请务必填写真实信息，提交后

系统会自动与学校数据库进行匹配

如自动通过可直接进行下一步校友卡办理

 

### 【未自动通过】
将会跳转到学籍认证环节

为提高审核效率

您可上传学位证或毕业证（其中之一）

 ![输入图片说明](https://images.gitee.com/uploads/images/2021/1011/133027_1e8fac67_9601161.png "屏幕截图.png")



 ![输入图片说明](https://images.gitee.com/uploads/images/2021/1011/133029_ee2a632c_9601161.png "屏幕截图.png")

 

 

 

 

 

# 卡片使用


 
![输入图片说明](https://images.gitee.com/uploads/images/2021/1011/133033_4339e257_9601161.png "屏幕截图.png")
 

 
![输入图片说明](https://images.gitee.com/uploads/images/2021/1011/133035_61a4eeb9_9601161.png "屏幕截图.png")
 

 

 # 后台管理


 ![输入图片说明](https://images.gitee.com/uploads/images/2021/1011/133038_b2c025e2_9601161.png "屏幕截图.png")

 

# 数据库


 ![输入图片说明](https://images.gitee.com/uploads/images/2021/1011/133041_c2855c71_9601161.png "屏幕截图.png")

 

# 前端代码


 
![输入图片说明](https://images.gitee.com/uploads/images/2021/1011/133043_36126172_9601161.png "屏幕截图.png")
 

# 后端代码
 

![输入图片说明](https://images.gitee.com/uploads/images/2021/1011/133047_885181f5_9601161.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/1011/133112_4d48d5e7_9601161.jpeg "ccplat-小程序QR.jpg")